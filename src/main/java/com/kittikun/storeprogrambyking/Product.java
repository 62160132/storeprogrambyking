/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittikun.storeprogrambyking;

import java.io.Serializable;

/**
 *
 * @author W10 Hs
 */
public class Product implements Serializable {

    private String id, brand, name;
    private Double price;
    private int amount;

    public Product(String id, String name, String brand, Double price, int amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PRODUCT : " + "ID = " + id + ", NAME = " + name + ", BRAND = " + brand + ", PRICE = " + price + ", AMOUNT = " + amount;
    }

}
